const canvas = document.getElementById("playspace")
const context = canvas.getContext("2d");

// Player character
let playerSprite = new Image();
playerSprite.src = "assets/img/recyclohomie.png";

// Obstacle
let boulderChan = new Image();
boulderChan.src = "assets/img/boulderchan.png";

// Obstacle 2
let hitmanmen = new Image();
hitmanmen.src = "assets/img/hitman.png";


const scale = 1;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;

const username = localStorage.getItem('username');
const score = localStorage.getItem('score');
if (username){
	let form = document.forms["helloForm"];
	form.style.display = 'none';
	let modal = document.getElementById("modal");
	let modalContent = modal.children[0],children[2];
	modal.style.display = "block";
	modalContent.innerHTML = "username: " + username + "<br>" + "score: " + score;
	let header = document.getElementById("main-header");
	header.innerHTML = "Hello " + username;
	let validateButton = document.getElementsByClassName("saved-data-accept")[0];
	let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
	validateButton.onclick = function(){
		window.location.href = "gameplay.html";
	}
	dismissButton.onclick = function(){
		modal.style.display = "none";
		form.style.display = "block";
		localStorage.clear();
	}
}
else {
	console.log("no data in localStorage, loading new session")
}
} else {
	console.log("Local storage is not supported.")
}

function validateForm(){
	var intro = document.forms["helloForm"]["name"].value;
	if (intro == "") {
			alert("I need to know your name so I can say Hello");
			return false;
	}
	else{
		alert("Hello there " + document.forms["helloForm"]["name"].value;
	}
	localStorage.setItem("username", intro);


// creating a constructor for objects
function GameObject(spritesheet, x, y, width, height) {
	this.spritesheet = spritesheet;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
}

// Creating a constructor for an animated player, containing 9 arguments
function PlayerAnim(spritesheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight, movementDir)
{
	this.spritesheet = spritesheet;
	this.sx = sx;
	this.sy = sy;
	this.sWidth = sWidth;
	this.sHeight = sHeight;
	this.dx = dx;
	this.dy = dy;
	this.dWidth = dWidth;
	this.dHeight = dHeight;
	this.movementDir = "None";
}

// creating a constructor for canvas decor
function WorldObject(x, y, width, height, colour) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.colour = colour;
}

let player = new PlayerAnim(playerSprite, 0, 0, 16, 18, 0, 0, 16, 18, 0);
let boulder = new GameObject(boulderChan, 75, 50, 60, 60);
let hitman = new GameObject(hitmanmen, 200, 75, 50, 50);
let bg = new WorldObject(0, 0, canvas.width, canvas.height, context.fillStyle = 'white') 

// Audio
var audio = new Audio('assets/audio/collide.wav');

// draw starting direction
player.movementDir = 0;

// Hold an input
function PlayerInput(input) {
	this.action = input; 
}

// Set up an input
let gameInput = new PlayerInput("None");
let boulderInput = new PlayerInput("None");

// get player input
function input(event){

if (event.type === "keydown") {
	switch (event.key) {
		case "a": // A
		gameInput = new PlayerInput("a");
		break;
		
		case "ArrowLeft": // left
		boulderInput = new PlayerInput("Left");
		break;
		
		case "w": // W
		gameInput = new PlayerInput("w");
		break;
		
		case "ArrowUp": // up
		boulderInput = new PlayerInput("Up");
		break;
		
		case "d": // D
		gameInput = new PlayerInput("d");
		break;
		
		case "ArrowDown": // down
		boulderInput = new PlayerInput("Down");
		break;
		
		case "s": // S
		gameInput = new PlayerInput("s");
		break;
		
		case "ArrowRight": // right
		boulderInput = new PlayerInput("Right");
		break;
		
		case "e":
		gameInput = new PlayerInput("e");
		break;
		
		case "q":
		gameInput = new PlayerInput("q");
		break;
		
		case "f":
		gameInput = new PlayerInput("f");
		break;
		
		default:
		gameInput = new PlayerInput("None"); // None
		boulderInput = new PlayerInput("None"); // None
	}
	
} else {
	gameInput = new PlayerInput("None"); // None
	boulderInput = new PlayerInput("None"); // None
}
}

function gameUpdate() {
	// Check for input
	// Player
	
	if (gameInput.action === "w") // up
	{
		player.dy -= 1;
		player.movementDir = 1;
	}
	else if (gameInput.action === "s") // down
	{
		player.dy += 1;
		player.movementDir = 0;
	}
	else if (gameInput.action === "a") // left
	{
		player.dx -= 1;
		player.movementDir = 2;
	}
	else if (gameInput.action === "d") // right
	{
		player.dx += 1;
		player.movementDir = 3;
	}
	else if (gameInput.action === "e") // Grow
	{
		// scale up
		player.width += 5;
		player.height += 5;
	}
	else if (gameInput.action === "q")
	{
		if (player.width > 1)
		{
			// scale Down
			player.width -= 5;
			player.height -= 5;
		}
		
	}
	
	else if (gameInput.action === "f")
	{
		// teleport player randomly
		player.dx = Math.floor(Math.random() * 250); // Random x position
		player.dy = Math.floor(Math.random() * 100); // Random y position
	}
	
	// boulder
	
	if (boulderInput.action === "Up") // up
	{
		boulder.y -= 1;
	}
	else if (boulderInput.action === "Down") // down
	{
		boulder.y += 1;
	}
	else if (boulderInput.action === "Left") // left
	{
		boulder.x -= 1;
	}
	else if (boulderInput.action === "Right") // right
	{
		boulder.x += 1;
	}
	
	// Collision detection
	collisionWall();
	collisionObject();
}

function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
	
	// Draw background
	context.fillRect (bg.x,
					bg.y,
					bg.width,
					bg.height,
					bg.colour);
					
					  
	// boulder
	context.drawImage(boulder.spritesheet, 
                      boulder.x,
                      boulder.y,
                      boulder.width,
                      boulder.height);
	
	// hitman
	context.drawImage(hitman.spritesheet, 
                      hitman.x,
                      hitman.y,
                      hitman.width,
                      hitman.height);
}


function drawFrame(frameX, frameY, canvasX, canvasY) {
	context.drawImage(playerSprite, frameX * width, frameY * height, width, height, canvasX, canvasY, scaledWidth, scaledHeight);
}


const walkLoop = [0, 1, 0, 2];
let currentLoopIndex = 0;
let frameCount = 0;

function step() {
	if (gameInput.action != "None") {
		frameCount++;
		
		if (frameCount >= 15){
			frameCount = 0;
			currentLoopIndex++;
			if (currentLoopIndex >= walkLoop.length) {
				currentLoopIndex = 0;
			}
		
		}
	}
	
	else {
		currentLoopIndex = 0;
	}
	
drawFrame(walkLoop[currentLoopIndex], player.movementDir, player.dx, player.dy);
}

window.requestAnimationFrame(step);

function collisionWall() {
	// check if player is colliding with walls of canvas
	if (player.dx > 260 || player.dx < 0
	 || player.dy > 106 || player.dy < -10)
	{
		bg.colour = context.fillStyle = 'red';
		console.log ("collision!");
		
		audio.play();
	}
	// check if boulderchan is colliding with walls of canvas
	else if (boulder.x > 260 || boulder.x < 0
	 || boulder.y > 106 || boulder.y < -10)
	{
		bg.colour = context.fillStyle = 'red';
		console.log ("collision!");
		
		audio.play();
	}
	else
	{
		// when leave collision of any sort, reset colour
		bg.color = context.fillStyle = 'white';
	}
}

function collisionObject() {
	// if player is colliding with boulder
	if (player.dx >= boulder.dx - 30 && player.dx <= boulder.dx + 30 
	&& player.dy >= boulder.y - 30 && player.dy <= boulder.y + 30)
	{
		bg.colour = context.fillStyle = 'red';
		console.log ("O-ouch");
		
		audio.play();
	}
	
	// if player is colliding with hitman
	else if (player.dx >= hitman.x - 30 && player.dx <= hitman.x + 30 
	&& player.dy >= hitman.y - 30 && player.dy <= hitman.y + 30)
	{
		bg.colour = context.fillStyle = 'red';
		console.log ("No! My son!");
		
		audio.play();
	}
	
	// if boulder is colliding with hitman
	else if (boulder.x >= hitman.x - 30 && boulder.x <= hitman.x + 30 
	&& boulder.y >= hitman.y - 30 && boulder.y <= hitman.y + 30)
	{
		bg.colour = context.fillStyle = 'red';
		console.log ("No! My son!");
		
		audio.play();
	}
}

// function that loads and updates all elements of the game
function gameloop() {
	gameUpdate();
    draw();
	step();
	window.requestAnimationFrame(gameloop);	
}

	// Handle browser animation
	window.requestAnimationFrame(gameloop);
	
	window.addEventListener('keydown', input);
	
	window.addEventListener('keyup', input);